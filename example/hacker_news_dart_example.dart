library hacker_news_dart;

import 'package:hacker_news_dart/src/request_service.dart';
import 'package:hacker_news_dart/src/story.dart';

main() {
  final Future<Item> sf = requestStory(8863);
  sf.then((Item story) {
    final comments = requestCommentsForStory(story);
    comments.forEach((f) {
      f.then((Item comment) {
        print(comment.id);
      });
    });
  });
}
