## Usage

A simple usage example:

```dart
  final Future<Item> sf = requestStory(8863);
  sf.then((Item story) {
    final comments = requestCommentsForStory(story);
    comments.forEach((f) {
      f.then((Item comment) {
        print(comment.id);
      });
    });
  });
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/agilob/hacker-news-dart