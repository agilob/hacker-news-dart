import 'dart:convert';
import 'dart:io';

import 'package:hacker_news_dart/hacker_news_dart.dart';
import 'package:test/test.dart';

final Map<String, String> envVars = Platform.environment;
final username = envVars["HN_USERNAME"];
final password = envVars["HN_PASSWORD"];

void main() {
  group('Request Single Models', () {
    setUp(() {});

    test('Request Story', () async {
      final HttpClientResponse response = await login(username, password);

      // expect(response, isNotNull);
      expect(response.statusCode, equals(200)); //it doesnt work, should be 302 when success
      // expect(response.cookies, isNotEmpty);
      response.transform(utf8.decoder).listen((data) {
        // print(data);
      });
    });
  });
}
