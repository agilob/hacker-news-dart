import 'package:hacker_news_dart/hacker_news_dart.dart';
import 'package:hacker_news_dart/src/pollopt.dart';
import 'package:hacker_news_dart/src/story.dart';
import 'package:hacker_news_dart/src/updates.dart';
import 'package:hacker_news_dart/src/user.dart';
import 'package:test/test.dart';

void main() {
  group('Request Single Models', () {
    setUp(() {});

    test('Request Story', () async {
      final Item story = await requestStory(8863);
      expect(story, isNotNull);
      expect(story.id, isNotNull);
      expect(story.by, isNotNull);
      expect(story.title, isNotNull);
      expect(story.score, greaterThan(1));
      expect(story.type, ItemType.story);
      expect(story.kids, isNotEmpty);
    });

    test('Request Poll', () async {
      final Item poll = await requestStory(126809);
      expect(poll, isNotNull);
      expect(poll.id, isNotNull);
      expect(poll.by, isNotNull);
      expect(poll.score, greaterThan(1));
      expect(poll.descendants, greaterThan(1));
      expect(poll.title, isNotNull);
      expect(poll.type, ItemType.poll);
      expect(poll.kids, isNotEmpty);
    });

    test('Request Poll Option', () async {
      final JsonPollOption pollOption = await requestPollOption(160705);
      expect(pollOption, isNotNull);
      expect(pollOption.id, isNotNull);
      expect(pollOption.by, isNotNull);
      expect(pollOption.poll, isNotNull);
      expect(pollOption.time, isNotNull);
      expect(pollOption.score, greaterThan(1));
      expect(pollOption.type, ItemType.pollopt);
    });

    test('Request Comment', () async {
      final Item comment = await requestComment(2921983);
      expect(comment, isNotNull);
      expect(comment.id, isNotNull);
      expect(comment.by, isNotNull);
      expect(comment.text, isNotNull);
      expect(comment.type, ItemType.comment);
      expect(comment.kids, isNotEmpty);
    });

    test('Request User', () async {
      final HaUser user = await requestUserInfo("jl");
      expect(user, isNotNull);
      expect(user.id, isNotNull);
      expect(user.about, isNotNull);
      expect(user.karma, greaterThan(1));
      expect(user.submitted, isNotEmpty);
    });
  });

  group('Request Stories By Group', () {
    test('Request Top Stories', () async {
      final Future<List<int>> stories = requestTopStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(42));
      });
    });

    test('Request Best Stories', () async {
      final Future<List<int>> stories = requestBestStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(42));
      });
    });

    test('Request New Stories', () async {
      final Future<List<int>> stories = requestNewStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(42));
      });
    });

    test('Request Ask Stories', () async {
      final Future<List<int>> stories = requestAskStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(1));
      });
    });

    test('Request Show Stories', () async {
      final Future<List<int>> stories = requestShowStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(1));
      });
    });

    test('Request Job Stories', () async {
      final Future<List<int>> stories = requestJobStoryIds();
      await stories.then((List<int> ids) {
        expect(ids.length, greaterThan(1));
      });
    });

    test('Request Stories By Ids', () async {
      final Future<List<int>> stories = requestJobStoryIds();
      await stories.then((List<int> ids) {
        requestStories(ids).forEach((f) {
          f.then((story) {
            expect(story, isNotNull);
            expect(story.id, isNotNull);
            expect(story.by, isNotNull);
            expect(story.type, isNotNull);
            expect(story.title, isNotNull);
          });
        });
      });
    });
  });

  group('Request Comments', () {
    test('Request List Of Comments', () async {
      Item story;
      await requestStory(8863).then((s) {
        story = s;
      });

      final commentsFutures = requestCommentsForStory(story);

      commentsFutures.forEach((future) {
        future.then((a) {
          expect(a, isNotNull);
          expect(a.id, isNotNull);
          expect(a.by, isNotNull);
          expect(a.text, isNotNull);
          expect(a.type, ItemType.comment);
        });
      });
    });
  });

  group('Item And Profile Updates', () {
    test('Request Updates', () async {
      await requestUpdates().then((JsonUpdates ju) {
        expect(ju, isNotNull);
        expect(ju.items, isNotNull);
        expect(ju.items.length, greaterThan(1));
        expect(ju.profiles, isNotNull);
        expect(ju.profiles.length, greaterThan(1));
      });
    });
  });
}
