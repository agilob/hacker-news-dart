## 0.1.5
- Move src generating dependencies to dev_dependencies

## 0.1.4
- Simplify some code and dartfmt all src files

## 0.1.3
- Make linter happier

## 0.1.2
- Just linter fixes

## 0.1.0

- Initial version
