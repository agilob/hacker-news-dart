import 'package:json_annotation/json_annotation.dart';

part 'story.g.dart';

@JsonSerializable()
class Item {
  final int id;
  final String by;
  final int time;
  final ItemType type;
  final int descendants;
  final List<int> kids;

  /// only when `type` is poll
  final List<int> parts;
  final int score;
  final String title;
  final String text;
  final String url;

  /// Only when `type` is comment
  int parent;

  Item(this.id, this.by, this.time, this.parts, this.type, this.descendants,
      this.kids, this.text, this.score, this.title, this.url);

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  Map<String, dynamic> toJson() => _$ItemToJson(this);

  @override
  String toString() => toJson().toString();
}

enum ItemType { story, comment, ask, job, poll, pollopt }
