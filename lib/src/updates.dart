import 'package:json_annotation/json_annotation.dart';

part 'updates.g.dart';

@JsonSerializable()
class JsonUpdates {
  List<int> items;
  List<String> profiles;

  JsonUpdates({this.items, this.profiles});

  factory JsonUpdates.fromJson(Map<String, dynamic> json) =>
      _$JsonUpdatesFromJson(json);

  Map<String, dynamic> toJson() => _$JsonUpdatesToJson(this);

  @override
  String toString() => toJson().toString();
}
