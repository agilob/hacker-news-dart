import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class HaUser {
  final String id;
  final String about;
  final int created;
  final int delay;
  final int karma;
  final List<int> submitted;

  HaUser(this.id, this.about, this.created, this.delay, this.karma,
      this.submitted);

  factory HaUser.fromJson(Map<String, dynamic> json) => _$HaUserFromJson(json);

  Map<String, dynamic> toJson() => _$HaUserToJson(this);

  @override
  String toString() => toJson().toString();
}
