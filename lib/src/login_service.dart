import 'dart:io';
import 'dart:convert';
import 'dart:async';

const _loginEndpoint = 'https://news.ycombinator.com/login';

final _httpClient = HttpClient();

Future<HttpClientResponse> login(final String acct, final String pw) async {
  return _httpClient
      .getUrl(Uri.parse(_loginEndpoint))
      .then((HttpClientRequest request) {
    final jsonMap = {'acct': acct, 'pw': pw, 'goto': 'news'};
    final jsonString = json.encode(jsonMap); // encode map to json
    final paramName = 'param'; // give the post param a name
    final formBody = paramName + '=' + Uri.encodeQueryComponent(jsonString);
    final bodyBytes = utf8.encode(formBody); // utf8 encode
    request.headers.set('Content-Length', bodyBytes.length.toString());
    request.headers.set(
      'Content-Type',
      'application/x-www-form-urlencoded',
    );
    request.headers.set('Access-Control-Allow-Origin', '*');
    request.add(bodyBytes);
    return request.close();
  });
}
