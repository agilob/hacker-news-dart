import 'dart:io';
import 'dart:convert';
import 'dart:async';

import 'package:hacker_news_dart/src/pollopt.dart';
import 'package:hacker_news_dart/src/story.dart';
import 'package:hacker_news_dart/src/updates.dart';
import 'package:hacker_news_dart/src/user.dart';
import 'package:uri/uri.dart';

const _topStories = 'https://hacker-news.firebaseio.com/v0/topstories.json';
const _bestStories = 'https://hacker-news.firebaseio.com/v0/beststories.json';
const _newStories = 'https://hacker-news.firebaseio.com/v0/newstories.json';

const _askStories = 'https://hacker-news.firebaseio.com/v0/askstories.json';
const _showStories = 'https://hacker-news.firebaseio.com/v0/showstories.json';
const _jobStories = 'https://hacker-news.firebaseio.com/v0/jobstories.json';

const _updates = 'https://hacker-news.firebaseio.com/v0/updates.json';

final _itemTemplate =
    UriTemplate('https://hacker-news.firebaseio.com/v0/item/{id}.json');
final _userTemplate =
    UriTemplate('https://hacker-news.firebaseio.com/v0/user/{username}.json');

final _upvoteTemplate = UriTemplate(
    'https://news.ycombinator.com/vote?id={id}&how=up&auth={auth}&goto=news');

List<Future<Item>> requestCommentsForStory(final Item story) {
  return story.kids.map((commentId) => requestComment(commentId)).toList();
}

Future<bool> upvoteItem(final int itemId, final String authCode) {
  final upvoteUrl = _upvoteTemplate.expand({'id': itemId, 'auth': authCode});
  final client = HttpClient();

  try {
    final completer = Completer<bool>();

    return client
        .getUrl(Uri.parse(upvoteUrl))
        .then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      if (response.statusCode >= 200 && response.statusCode < 400) {
        completer.complete(true);
      } else {
        completer.complete(false);
      }
      return completer.future;
    });
  } finally {
    client.close();
  }
}

Future<Item> requestStory(final int id) {
  final storyUrl = _itemTemplate.expand({'id': id});
  final client = HttpClient();

  try {
    final completer = Completer<Item>();

    client.getUrl(Uri.parse(storyUrl)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        completer.complete(Item.fromJson(json.decode(data)));
      });
    });
    return completer.future;
  } finally {
    client.close();
  }
}

Future<List<int>> requestTopStoryIds() {
  return _futureOfStories(_topStories);
}

Future<List<int>> requestBestStoryIds() {
  return _futureOfStories(_bestStories);
}

Future<List<int>> requestNewStoryIds() {
  return _futureOfStories(_newStories);
}

Future<List<int>> requestAskStoryIds() {
  return _futureOfStories(_askStories);
}

Future<List<int>> requestShowStoryIds() {
  return _futureOfStories(_showStories);
}

Future<List<int>> requestJobStoryIds() {
  return _futureOfStories(_jobStories);
}

List<Future<Item>> requestStories(final List<int> ids) {
  return _requestFullStories(ids);
}

List<Future<Item>> _requestFullStories(final List<int> ids) {
  return ids.map((id) => requestStory(id)).toList();
}

List<Future<JsonPollOption>> requestPollOptions(final List<int> ids) {
  return ids.map((id) => requestPollOption(id)).toList();
}

Future<JsonPollOption> requestPollOption(final int id) {
  final storyUrl = _itemTemplate.expand({'id': id});

  final client = HttpClient();

  try {
    final completer = Completer<JsonPollOption>();

    client.getUrl(Uri.parse(storyUrl)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        completer.complete(JsonPollOption.fromJson(json.decode(data)));
      });
    });
    return completer.future;
  } finally {
    client.close();
  }
}

Future<JsonUpdates> requestUpdates() {
  final client = HttpClient();
  try {
    final completer = Completer<JsonUpdates>();

    client.getUrl(Uri.parse(_updates)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        completer.complete(JsonUpdates.fromJson(json.decode(data)));
      });
    });
    return completer.future;
  } finally {
    client.close();
  }
}

Future<HaUser> requestUserInfo(final String username) {
  final userUrl = _userTemplate.expand({'username': username});

  final client = HttpClient();

  try {
    final completer = Completer<HaUser>();

    client.getUrl(Uri.parse(userUrl)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        completer.complete(HaUser.fromJson(json.decode(data)));
      });
    });
    return completer.future;
  } finally {
    client.close();
  }
}

Future<Item> requestComment(final int id) {
  final storyUrl = _itemTemplate.expand({'id': id});

  final client = HttpClient();

  try {
    final completer = Completer<Item>();

    client.getUrl(Uri.parse(storyUrl)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        completer.complete(Item.fromJson(json.decode(data)));
      });
    });
    return completer.future;
  } finally {
    client.close();
  }
}

Future<List<int>> _futureOfStories(final String url) {
  final client = HttpClient();
  final completer = Completer<List<int>>();

  try {
    client.getUrl(Uri.parse(url)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) {
      response.transform(utf8.decoder).listen((data) {
        final list = (jsonDecode(data)).cast<int>();
        completer.complete(list);
      });
    });
  } finally {
    client.close();
  }

  return completer.future;
}
