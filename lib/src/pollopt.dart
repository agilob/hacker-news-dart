import 'package:hacker_news_dart/src/story.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pollopt.g.dart';

@JsonSerializable()
class JsonPollOption {
  final int id;
  final String by;
  final int time;
  final ItemType type;
  final int poll;
  final int score;
  final String text;

  JsonPollOption(
      this.id, this.time, this.by, this.type, this.poll, this.score, this.text);

  factory JsonPollOption.fromJson(Map<String, dynamic> json) =>
      _$JsonPollOptionFromJson(json);

  Map<String, dynamic> toJson() => _$JsonPollOptionToJson(this);

  @override
  String toString() => toJson().toString();
}
